package com.example.mvcdemo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.mvcdemo.model.Alien;

@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String home()
	{
		return "index";
	}

//	@RequestMapping("add")
//	public String add(HttpServletRequest req)
//	{
//		int i=Integer.parseInt(req.getParameter("num1"));
//		int j=Integer.parseInt(req.getParameter("num2"));
//		int num3=i+j;
//		HttpSession session=req.getSession();
//		session.setAttribute("num3",num3);
//		return "result.jsp";
//		
//	}
	
//	@RequestMapping("add")
//	public String add(@RequestParam("num1") int i,@RequestParam("num2")  int j,HttpSession session)
//	{
//		int num3=i+j;
//		session.setAttribute("num3", num3);
//		return "result.jsp";
//	}
	
//	@RequestMapping("add")
//	public ModelAndView add(@RequestParam("num1") int i,@RequestParam("num2") int j)
//	{
//		ModelAndView mv=new ModelAndView();
////		ModelAndView mv=new ModelAndView("result");we call constructor
//		mv.setViewName("result");
//		int num3=i+j;
//		mv.addObject("num3", num3);
//		return mv;
//		
//	}
	
//	@RequestMapping("add")
//	public String add(@RequestParam("num1") int i,@RequestParam("num2") int j,Model m)
//	{
//		int num3=i+j;
//		m.addAttribute("num3", num3);
//		return "result";
//		
//	}
	
	@RequestMapping("add")
	public String add(@RequestParam("num1") int i,@RequestParam("num2") int j,ModelMap m)
	{
		int num3=i+j;
		m.addAttribute("num3", num3);
		return "result";
		
	}
	
//	@RequestMapping("addAlien")
//	public String addAlien(@RequestParam("aid") int id,@RequestParam("aname") String name,Model m)
//	{
//		Alien al=new Alien();
//		al.setId(id);
//		al.setName(name);
//		m.addAttribute("alien", al);
//		return "result";
//		
//	}
	
	@RequestMapping("addAlien")
	public String addAlien(@ModelAttribute("ali") Alien al)
	{
		return "result";
		
	}
}
